USE master
EXEC sp_configure "max server memory (MB)", {{cfg.max_memory}}
EXEC sp_configure "min server memory (MB)", {{cfg.min_memory}}
RECONFIGURE WITH OVERRIDE
GO